if __name__ == '__main__':
    N = int(input('Give an integer: '))
    list_of_ints = []
    command = ""

    for i in range(0, N):
        command = str(input('Give a command: ')).split()
        if command[0] == 'insert':
            list_of_ints.insert(int(command[1]), int(command[2]))
        elif command[0] == 'print':
            print(list_of_ints)
        elif command[0] == 'remove':
            list_of_ints.remove(int(command[1]))
        elif command[0] == 'append':
            list_of_ints.append(int(command[1]))
        elif command[0] == 'sort':
            list_of_ints.sort()
        elif command[0] == 'pop':
            list_of_ints.pop()
        elif command[0] == 'reverse':
            list_of_ints.reverse()


print()
