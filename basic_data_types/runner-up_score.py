if __name__ == '__main__':
    n = int(input('Give an integer 2 - 10: '))

    if 2 <= n <= 10:
        array = list(map(int, input(': ').split()[:n]))
        if -100 <= len(array) <= 100:
            print(sorted(set(array))[-2])
        else:
            print('Array not in range: -100 - 100!')
    else:
        print('N not in range: 2 - 10!')
