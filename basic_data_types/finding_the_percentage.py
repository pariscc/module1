if __name__ == '__main__':
    n = int(input('Give an integer: '))

    if 2 <= n <= 10:
        student_marks = {}

        for i in range(n):
            name, *marks = input(': ').split()
            scores = list(map(float, marks))
            student_marks[name] = sum(scores) / float(len(scores))

        query_name = input('Name: ')
        print(student_marks[query_name])
