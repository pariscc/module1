if __name__ == '__main__':
    x = int(input('Give x: '))
    y = int(input('Give y: '))
    z = int(input('Give z: '))
    n = int(input('Give n: '))

    list_of_coordinates = [[i, j, k] for i in range(x + 1)
                           for j in range(y + 1)
                           for k in range(z + 1)
                           if i + j + k != n]
    print(list_of_coordinates)
