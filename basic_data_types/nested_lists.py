if __name__ == '__main__':
    n = int(input('Give an integer: '))

    if 2 <= n <= 5:
        names_and_grades = []
        grades = []

        for user_input in range(n):
            names_and_grades.append([input('Name: '), float(input('Grade: '))])
            grades.append(names_and_grades[user_input][1])

        lowest = []
        second_lowest = []

        for index in names_and_grades:
            if index[1] == min(names_and_grades):
                lowest.append(index)

        for grade in lowest:
            del names_and_grades[names_and_grades.index(grade)]
            del grades[grades.index(grade[1])]

        for index in names_and_grades:
            if index[1] == min(grades):
                second_lowest.append(index)

        for i in sorted(second_lowest):
            print(i[0])
    else:
        print('N must be in in range 2 <= N <= 5!')
