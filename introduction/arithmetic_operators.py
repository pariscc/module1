a = 0
b = 0


a = int(input('First integer: '))
b = int(input('Second integer: '))


if a not in range(1, 10 ** 10 + 1) or b not in range(1, 10 ** 10 + 1):
    print('That\'s out of range: 1 to 10^10')
else:
    print(a + b)
    print(a - b)
    print(a * b)
