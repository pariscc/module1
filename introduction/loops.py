N = 0


N = int(input('Give an integer between 1 and 20: '))

if N not in range(1, 20 + 1):
    print('That\'s not in range: 1 to 20!')
else:
    for i in range(0, N):
        print(i ** 2)
