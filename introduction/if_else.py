n = 0


try:
    n = int(input('Give an integer: '))

    if n not in range(1, 100 + 1):
        print('{} is not in range: 1 to 100!'.format(n))
    else:
        if n % 2 != 0:
            print('Weird')
        else:
            if n in range(2, 5 + 1):
                print('Not Weird')
            if n in range(6, 20 + 1):
                print('Weird')
            if n > 20:
                print('Not Weird')
except ValueError:
    print('{} is not an integer!'.format(n))
