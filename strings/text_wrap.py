def wrap(string, max_width):
    for i in range(0, len(string) + 1, max_width):
        print(string[i:max_width + i])
    return None


if __name__ == '__main__':
    try:
        string = str(input('Give a string: '))
        max_width = int((input('Give max width: ')))
        if (0 <= len(string) <= 1000) and (0 <= max_width <= len(string)):
            wrap(string, max_width)
    except ValueError:
        print('Wrong input!')
