def print_formatted(number):
    binary_width = len(bin(number)) - 2
    for i in range(1, number + 1):
        print('{no: > {width}d} {no: > {width}o} {no: > {width}X} \
              {no: > {width}b}'.format(width=binary_width, no=i))
    return None


if __name__ == '__main__':
    try:
        number = int(input('Give a number: '))
        if 1 <= number <= 99:
            print_formatted(number)
    except ValueError:
        print('Wrong input!')
