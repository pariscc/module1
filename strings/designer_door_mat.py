if __name__ == '__main__':
    try:
        n, m = list(map(int, input(': ').split()))

        if (5 <= n <= 101) and (15 <= m <= 303):
            for i in range(n // 2):
                print(('.|.' * i + '.|.' + '.|.' * i).center(m, '-'))

            print('WELCOME'.center(m, '-'))

            for i in range(n // 2 - 1, -1, -1):
                print(('.|.' * i + '.|.' + '.|.' * i).center(m, '-'))
    except ValueError:
        print('Wrong input!')
