def mutate_string(string, position, character):
    return string[:position] + character + string[position + 1:]


if __name__ == '__main__':
    try:
        string = str(input('Give a string: '))
        position = int(input('Give position: '))
        character = str(input('Give character: '))

        print(mutate_string(string, position, character))
    except ValueError:
        print('Wrong input!')
