def print_rangoli(size):
    for i in list(range(size - 1, 0, -1)) + list(range(size)):
        line = ('-'.join([chr(ord('a') + i)
                          for i in range(size - 1, i - 1, -1)] +
                         [chr(ord('a') + i) for i in range(i + 1, size)]))
        print(line.center(4 * size - 3, '-'))
    return None


if __name__ == '__main__':
    try:
        n = int(input('Give a integer: '))
        if 0 <= n <= 27:
            print_rangoli(n)
        else:
            print('N - [0, 27]')
    except ValueError:
        print('Wrong input!')
