def count_substring(string, substring):
    if 2 <= len(string) <= 200:
        try:
            ascii_string = string.encode('ascii')
            ascii_substring = substring.encode('ascii')
            counter = 0
            for i in range(len(ascii_string)):
                if string[i:i + len(ascii_string)] == ascii_substring:
                    counter += 1
            return counter
        except UnicodeEncodeError:
            print('That\'s not an ascii string!')


if __name__ == '__main__':
    try:
        string = str(input('Give a string: '))
        substring = str(input('Give substring: '))
        print(count_substring(string, substring))
    except ValueError:
        print('That\'s not a string!')
