if __name__ == '__main__':
    try:
        string = str(input('Give a string: '))
        if 0 <= len(string) <= 1000:
            print(any(char.isalnum() for char in string))
            print(any(char.isalpha() for char in string))
            print(any(char.isdigit() for char in string))
            print(any(char.islower() for char in string))
            print(any(char.isupper() for char in string))
    except ValueError:
        print('That\'s not a string!')
