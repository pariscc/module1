def swap_case(string):
    new_string = [i.lower() if i.isupper() else i.upper() for i in string]
    return ''.join(new_string)


try:
    string = str(input('Give a string: '))
    print(swap_case(string))
except ValueError:
    print('That\'s not a string!')
