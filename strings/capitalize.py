# def capitalize(string):
    # return string.title()


# without title() method
def capitalize(string):
    line = ''.join([string[0].upper()] + [string[i].upper()
                                          if string[i - 1].isspace()
                                          else string[i]
                                          for i in range(1, len(string))])
    return line


if __name__ == '__main__':
    try:
        name = str(input('Give forname and surname: '))
        if (0 <= len(name) <= 1000) and (name.isalpha):
            print(capitalize(name))
    except ValueError:
        print('Wrong input!')
