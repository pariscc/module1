thickness = int(input('Give an odd number: '))

if (thickness % 2 != 0) and (0 <= thickness <= 50):
    c = 'H'
    # Top Core
    for i in range(thickness):
        print((c * i).rjust(thickness - 1) + c + (c * i).ljust(thickness - 1))

    # Top Pillars
    for i in range(thickness + 1):
        print((c * thickness).center(thickness * 2) +
              (c * thickness).center(thickness * 6))

    # Middle Belt
    for i in range((thickness + 1) // 2):
        print((c * thickness * 5).center(thickness * 6))

    # Bottom Pillars
    for i in range(thickness + 1):
        print((c * thickness).center(thickness * 2) +
              (c * thickness).center(thickness * 6))

    # Bottom Core
    for i in range(thickness):
        print(((c * (thickness - i - 1)).rjust(thickness) + c +
              (c * (thickness - i - 1)).ljust(thickness)).rjust(thickness * 6))
