def print_full_name(first_name, last_name):
    print(f'Hello {first_name} {last_name}! You just delved into python.')
    return None


if __name__ == '__main__':
    print_full_name('Ross', 'Taylor')
